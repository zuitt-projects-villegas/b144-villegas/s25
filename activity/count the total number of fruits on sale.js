db.fruits.aggregate(
	[
		{ $match: { onSale:true } },
		{ $count: "available_fruits" }
	])