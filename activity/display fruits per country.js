db.fruits.aggregate([
    {$unwind: "$origin" },
    {$group: {_id: "$origin", fruits: {$sum: 1}}}
])
    