db.fruits.aggregate(
	[
		{ $match: { onSale:true } },
		{ $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" } } },
		{ $sort: { _id: -1 } }
		
	]
)